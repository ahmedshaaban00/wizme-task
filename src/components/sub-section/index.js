import React from 'react';

import './_sub-section.scss';
import Features from '../features';
import Button from '../button';

const IntroSection = props =>
  (<section className="sub-section" id={props.type}>
    <div className="container-fluid">
      <div className="row"> 
        <div className="col-md-8 sub-section-image d-flex flex-column justify-content-center align-items-center">
          <p>meet the first smart</p>
          <p>auction for venue booking</p>
        </div>
        <div className="col-md-4 sub-section-content">
          <h3>for {props.type}</h3>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
          <Features data={['Benefit 1', 'Benefit 2', 'Benefit 3']}/>
          <Button link="#0" className="" text="Apply" />
        </div>
      </div>
    </div>
  </section>);

export default IntroSection;
