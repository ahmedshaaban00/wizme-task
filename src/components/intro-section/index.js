import React from 'react';

import './_intro-section.scss';

class IntroSection extends React.Component {
  render() {
    return (<section className="intro-section">
      <div className="intro-section-nav">
        <a href="#bookers" className="intro-section-nav-item">Bookers</a>
      </div>
      <div className="intro-section-content container d-flex flex-column justify-content-center align-items-center">
        <p>meet the first smart<br/>auction for venue booking</p>
        <p>everyobody wins<br/>- wizme -</p>
      </div>
      <div className="intro-section-nav">
        <a href="#venues" className="intro-section-nav-item">Venues</a>
      </div>
    </section>);
  }
}

// const IntroSection = props =>
//   (<section className="intro-section">
//     <div className="intro-section-nav">
//       <a onClick={goToSection('bookers')} className="intro-section-nav-item">Bookers</a>
//     </div>
//     <div className="intro-section-content container d-flex flex-column justify-content-center align-items-center">
//       <p>meet the first smart<br/>auction for venue booking</p>
//       <p>everyobody wins<br/>- wizme -</p>
//     </div>
//     <div className="intro-section-nav">
//       <a href="#venues" className="intro-section-nav-item">Venues</a>
//     </div>
//   </section>);

export default IntroSection;
