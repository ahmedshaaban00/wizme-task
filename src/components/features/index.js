import React from 'react';

import './_features.scss';

const Features = props =>
  (<div className="row features-list">
    <div className="col-12">
      <ul className="features-list">
        {props.data.map((item, i) =>
          (<li className="features-list-item" key={i}>
            <div className="feature-shape"></div>
            {item}
          </li>),
        )}
      </ul>
    </div>
  </div>);

export default Features;
