import React from 'react';

import './_header.scss';

const Header = () =>
  (<header className="header fixed-top">
    <div className="container">
      <div className="row">
        <div className="col-12">
          <div className="header-content text-center">
            <span className="header-logo">
              Wizme
            </span>
          </div>
        </div>
      </div>
    </div>
  </header>);

export default Header;
