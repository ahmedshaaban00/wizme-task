import React from 'react';
import Icon from '../icon';
import Github from '../icon/github.icon';
import './_footer.scss';

const Footer = () =>
  (<footer className="footer fixed-bottom">
    <div className="container">
      <div className="row">
        <div className="col-12">
          <div className="footer-content text-center">
            <a href="#0" className="footer-content-item">
              About
            </a>
          </div>
        </div>
      </div>
    </div>
  </footer>);

export default Footer;
