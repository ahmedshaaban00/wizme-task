/* global graphql */

import React from 'react';
import IntroSection from '../components/intro-section';
import SubSection from '../components/sub-section';

const IndexPage = props =>
  (<main>
    <IntroSection />
    <SubSection type="venues"/>
    <SubSection type="bookers"/>
  </main>);

export default IndexPage;

export const pageQuery = graphql`
  query IndexQuery {
    allDataJson {
      edges {
        node {
          features {
            title
          }
          howTo {
            title
          }
        }
      }
    }
  }
`;
